import 'dart:io';
import 'dart:isolate';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ble_lib/flutter_ble_lib.dart';
import 'package:health_demo/service/location_service.dart';
import 'package:health_demo/widget/stat-card.dart';
import 'package:image_picker/image_picker.dart';
import 'package:location/location.dart' hide PermissionStatus;
import 'package:location_permissions/location_permissions.dart';
import 'package:observable/observable.dart';
import 'package:android_alarm_manager/android_alarm_manager.dart';

void main() async {
  // WidgetsFlutterBinding.ensureInitialized();
  // await AndroidAlarmManager.initialize();
  runApp(MyApp());
  // Isolate.spawn((message) {
  //   bleManager.startPeripheralScan();
  // }, '');
}

//TODO testing
bool isScanning = true;
BleManager bleManager;

// void printHello() {
//   final DateTime now = DateTime.now();
//   final int isolateId = Isolate.current.hashCode;
//   print("[$now] Hello, world! isolate=${isolateId} function='$printHello'");
// }

// void printHello1() {
//   final DateTime now = DateTime.now();
//   // final int isolateId = Isolate.current.hashCode;
//   print("[$now]");

//   if (isScanning) {
//     if (bleManager != null) {
//       bleManager.stopPeripheralScan();
//     }
//     isScanning = false;
//   } else {
//     bleManager = BleManager();
//     bleManager.createClient();
//     backgroundScan(bleManager, (event) {
//       print(event);
//     });
//     isScanning = true;
//   }

//   print(isScanning);
// }

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Health App Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

// void startAlarm() async {
//   AndroidAlarmManager.periodic(const Duration(seconds: 10), 0, printHello1);
// }

// void backgroundScan(BleManager manager, Function callback) async {
//   manager.startPeripheralScan().listen((event) {
//     // callback(event.peripheral.name);
//     print('scan started');
//     print(event.peripheral.name);
//   });
//   // print(callback);
//   print('scan started');
//   await AndroidAlarmManager.oneShot(Duration(seconds: 10), 1, () {
//     stopBackgroundScan(manager);
//   });
// }

// void stopBackgroundScan(BleManager manager) {
//   manager.stopPeripheralScan();
//   print('scan stopped');
// }

class _MyHomePageState extends State<MyHomePage> {
  static const platform = const MethodChannel('locationMethodChannel');
  BleManager bleManager;
  ObservableMap<String, ScanResult> peripherals = ObservableMap();
  int pulse, oxyLevel, systol, diastol, glucoseLevel;
  LocationService locationService;
  LocationData locationData;
  File image;
  // bool isScanning = false;

  String _metadata = 'Unknown battery level.';

  Future<String> addLocationToMetadata(String image) async {
    String metadata;
    try {
      final String result =
          await platform.invokeMethod('addLocationToMetadata', [
        image,
        locationData.altitude,
        locationData.latitude,
        locationData.longitude,
      ]);
      metadata = result;
    } on PlatformException catch (e) {
      metadata = "Failed to set metadata: '${e.message}'.";
    }

    setState(() {
      _metadata = metadata;
    });
    return metadata;
  }

  @override
  void initState() {
    LocationPermissions().checkPermissionStatus().then((permissionStatus) {
      if (permissionStatus != PermissionStatus.granted) {
        LocationPermissions().requestPermissions();
        bleManager = BleManager();
        bleManager.createClient();
      } else {
        bleManager = BleManager();
        bleManager.createClient();
      }
    });
    super.initState();
    // startAlarm();
    initializeLocationService();
  }

  initializeLocationService() {
    locationService = LocationService();
    locationService.onLocationChanged.listen((locationData) {
      setState(() {
        this.locationData = locationData;
      });
    });
    setState(() {
      locationData = locationService.lastLocationData;
    });
  }

  void askForImage(bool camera) {
    final picker = ImagePicker()
        .getImage(source: camera ? ImageSource.camera : ImageSource.gallery);
    picker.then((value) {
      setState(() {
        image = File(value.path);
        print("=====path=====");
        print(value.path);
        // useNetworkImage = false;
        addLocationToMetadata(value.path).then((value) {
          print(value);
        });
      });
    });
  }

  _startScan() {
    bleManager.startPeripheralScan().listen((scanResult) {
      peripherals.putIfAbsent(scanResult.peripheral.identifier, () {
        scanResult.peripheral
            .observeConnectionState(
                completeOnDisconnect: true, emitCurrentValue: false)
            .listen((connectionState) {
          print('======connectionState======');
          print(scanResult.peripheral.name);
          print(connectionState);
          if (connectionState == PeripheralConnectionState.connected) {
            scanResult.peripheral
                .discoverAllServicesAndCharacteristics()
                .then((_) {
              // scanResult.peripheral.services().then((services) {
              //   services.forEach((service) {
              //     service.characteristics().then((characteristics) {
              //       characteristics.forEach((characteristic) {
              //         print('============================');
              //         print(characteristic.service.uuid);
              //         print(characteristic.uuid);
              //         print(characteristic.isNotifiable);
              //         print(characteristic.isReadable);
              //         print(characteristic.isIndicatable);
              //         print(characteristic.isWritableWithResponse);
              //         print(characteristic.isWritableWithoutResponse);
              //         // if (characteristic.isReadable) {
              //         //   characteristic.read().then((value) {
              //         // characteristic.descriptors().then((descriptors) {
              //         //   print('descriptor length: ${descriptors.length}');
              //         //   descriptors.forEach((descriptor) {
              //         //     descriptor.read().then((value) {
              //         //       print(descriptor.uuid);
              //         //       print(value);
              //         //     });
              //         //   });
              //         // });
              //         //     print(value);
              //         //   });
              //         // }
              //       });
              //     });
              //   });

              // scanResult.peripheral
              //     .monitorCharacteristic(
              //         '00001801-0000-1000-8000-00805f9b34fb',
              //         '00002a05-0000-1000-8000-00805f9b34fb')
              //     .listen((value) {
              //   print('00002a05-0000-1000-8000-00805f9b34fb');
              //   print(value);
              // });

              // scanResult.peripheral
              //     .readCharacteristic('0000fed1-0000-3512-2118-0009af100700',
              //         '0000fed2-0000-3512-2118-0009af100700')
              //     .then((value) {
              //   print('0000fed2-0000-3512-2118-0009af100700');
              //   print(value);
              // });

              // scanResult.peripheral
              //     .monitorCharacteristic(
              //         '0000fed1-0000-3512-2118-0009af100700',
              //         '0000fed2-0000-3512-2118-0009af100700')
              //     .listen((value) {
              //   print('0000fed2-0000-3512-2118-0009af100700');
              //   print(value);
              // });

              // scanResult.peripheral
              //     .readCharacteristic('0000fee1-0000-1000-8000-00805f9b34fb',
              //         '0000fedd-0000-1000-8000-00805f9b34fb')
              //     .then((value) {
              //   print('0000fedd-0000-1000-8000-00805f9b34fb');
              //   print(value);
              // });

              // scanResult.peripheral
              //     .monitorCharacteristic(
              //         '0000fee1-0000-1000-8000-00805f9b34fb',
              //         '0000fedd-0000-1000-8000-00805f9b34fb')
              //     .listen((value) {
              //   print('0000fedd-0000-1000-8000-00805f9b34fb');
              //   print(value);
              // });

              // scanResult.peripheral
              //     .readCharacteristic('0000fee1-0000-1000-8000-00805f9b34fb',
              //         '0000fede-0000-1000-8000-00805f9b34fb')
              //     .then((value) {
              //   print('0000fede-0000-1000-8000-00805f9b34fb');
              //   print(value);
              // });

              // scanResult.peripheral
              //     .monitorCharacteristic(
              //         '0000fee1-0000-1000-8000-00805f9b34fb',
              //         '0000fede-0000-1000-8000-00805f9b34fb')
              //     .listen((value) {
              //   print('0000fede-0000-1000-8000-00805f9b34fb');
              //   print(value);
              // });
              // });

              scanResult.peripheral
                  .monitorCharacteristic('cdeacb80-5235-4c07-8846-93a37ee6b86d',
                      'cdeacb81-5235-4c07-8846-93a37ee6b86d')
                  .listen((event) {
                print(event.service.uuid);
                print(event.uuid);
                print(event.value);
                if (event.value.length == 4) {
                  setState(() {
                    pulse = event.value[1];
                    oxyLevel = event.value[2];
                  });
                }
              });

              scanResult.peripheral
                  .monitorCharacteristic('0000fff0-0000-1000-8000-00805f9b34fb',
                      '0000fff4-0000-1000-8000-00805f9b34fb')
                  .listen((event) {
                print(event.service.uuid);
                print(event.uuid);
                print(event.value);
                if (event.value[0] == 30) {
                  setState(() {
                    systol = event.value[2];
                    diastol = event.value[4];
                    pulse = event.value[8];
                  });
                }
                if (event.value[0] == 85 && event.value.length == 12) {
                  setState(() {
                    glucoseLevel = event.value[9];
                  });
                }
              });
            });
          }
        });
        setState(() {
          scanResult.peripheral.connect();
        });
        print('doConnect');
        return scanResult;
      });
    });
    setState(() {
      isScanning = true;
      // Future.delayed(Duration(seconds: 10), () {
      //   bleManager.stopPeripheralScan();
      //   isScanning = false;
      // });
    });
  }

  _stopScan() {
    // bleManager.stopPeripheralScan();
    peripherals.forEach((key, value) {
      value.peripheral.disconnectOrCancelConnection();
    });
    setState(() {
      isScanning = false;
    });
  }

  refresh() {
    bleManager.stopPeripheralScan();
    _stopScan();
    LocationPermissions().checkPermissionStatus().then((permissionStatus) {
      if (permissionStatus != PermissionStatus.granted) {
        LocationPermissions().requestPermissions();
        bleManager = BleManager();
        bleManager.createClient();
      } else {
        bleManager = BleManager();
        bleManager.createClient();
      }
    });
    locationService.locationData().then((location) {
      setState(() {
        locationData = location;
      });
    });
    askForImage(false);
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(widget.title),
        ),
        body: ListView(
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 18.0),
                          child: StatCard(
                            name: "Oxygen Level",
                            stat: oxyLevel ?? 127,
                            unit: '%',
                            max: 100,
                            secondLowerLimit: 95,
                            firstLowerLimit: 90,
                            width: 0.4,
                          ),
                        ),
                        StatCard(
                          name: "Pulse",
                          stat: pulse ?? 9999,
                          unit: 'bpm',
                          max: 255,
                          width: 0.4,
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        StatCard(
                          name: "Systol",
                          stat: systol ?? 9999,
                          unit: 'mmHg',
                          max: 180,
                          firstColor: Colors.yellow,
                          secondColor: Colors.green,
                          thirdColor: Colors.red,
                          firstLowerLimit: 90,
                          secondLowerLimit: 130,
                          width: 0.4,
                        ),
                        Text(
                          '/',
                          style: TextStyle(fontSize: 44),
                        ),
                        StatCard(
                          name: "Diastol",
                          stat: diastol ?? 9999,
                          unit: 'mmHg',
                          max: 140,
                          firstColor: Colors.yellow,
                          secondColor: Colors.green,
                          thirdColor: Colors.red,
                          firstLowerLimit: 60,
                          secondLowerLimit: 85,
                          width: 0.4,
                        ),
                      ],
                    ),
                    StatCard(
                      name: 'Blood Glucose',
                      stat: glucoseLevel ?? 9999,
                      unit: 'mg/dL',
                      max: 180,
                      firstColor: Colors.yellow,
                      secondColor: Colors.green,
                      thirdColor: Colors.red,
                      firstLowerLimit: 72,
                      secondLowerLimit: 110,
                    ),
                    RaisedButton(
                      onPressed: isScanning
                          ? null
                          : () {
                              _startScan();
                            },
                      child: Text('Start Scanning'),
                      disabledColor: Colors.grey,
                      color: Colors.lightBlue,
                    ),
                    RaisedButton(
                      onPressed: isScanning
                          ? () {
                              _stopScan();
                            }
                          : null,
                      child: Text('Stop Scanning'),
                      // color: Colors.blueGrey,
                      disabledColor: Colors.grey,
                    ),
                    RaisedButton(
                      onPressed: () {
                        refresh();
                      },
                      child: Text('Refresh'),
                      // color: Colors.blueGrey,
                      disabledColor: Colors.grey,
                    ),
                    Text('Click refresh if scanning doesn\'t start.'),
                    locationData != null
                        ? Column(
                            children: [
                              Text(
                                  'latitude: ${locationData.latitude.toString()}'),
                              Text(
                                  'longitude: ${locationData.longitude.toString()}'),
                              Text(
                                  'altitude: ${locationData.altitude.toString()}'),
                              Text(
                                  'accuracy: ${locationData.accuracy.toString()}'),
                              Text(
                                  'heading: ${locationData.heading.toString()}'),
                              Text(_metadata),
                            ],
                          )
                        : Text('Location not initialized'),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
