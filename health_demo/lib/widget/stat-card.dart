import 'package:flutter/material.dart';

class StatCard extends StatelessWidget {
  final String name;
  final String unit;
  int stat = 9999, max = -9999;
  final int secondLowerLimit, firstLowerLimit;
  final Color firstColor, secondColor, thirdColor;
  double width;

  StatCard(
      {@required this.name,
      @required this.stat,
      @required this.unit,
      @required this.max,
      this.secondLowerLimit,
      this.firstLowerLimit,
      this.firstColor = Colors.red,
      this.secondColor = Colors.yellow,
      this.thirdColor = Colors.green,
      this.width}); //constructor

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        width: width != null
            ? MediaQuery.of(context).size.width * width
            : MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                name,
                style: TextStyle(fontSize: 17),
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  stat != null && stat <= max
                      ? Text(
                          '$stat',
                          style: TextStyle(
                              fontSize: 25,
                              color: secondLowerLimit != null
                                  ? stat >= secondLowerLimit
                                      ? thirdColor
                                      : stat >= firstLowerLimit
                                          ? secondColor
                                          : firstColor
                                  : Colors.black),
                        )
                      : Text(
                          '--',
                          style: TextStyle(fontSize: 32),
                        ),
                  Text(
                    unit,
                    style: TextStyle(fontSize: 25),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
