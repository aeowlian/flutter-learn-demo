import 'package:flutter/scheduler.dart';
import 'package:location/location.dart';

class LocationService {
  static final LocationService _locationService =
      LocationService._privateConstructor();

  Location _location;
  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  LocationData _locationData;

  factory LocationService() {
    return _locationService;
  }

  LocationService._privateConstructor() {
    _location = Location();
    initialize();
  }

  initialize() async {
    _location.changeSettings(interval: 500, accuracy: LocationAccuracy.high);
    _serviceEnabled = await _location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await _location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await _location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await _location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _locationData = await _location.getLocation();
    try {
      _location.onLocationChanged.listen((event) {
        _locationData = event;
      });
    } catch (error) {
      initialize();
    }
  }

  Future<bool> requestService() async {
    _serviceEnabled = await _location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await _location.requestService();
      if (!_serviceEnabled) {
        return false;
      }
    }
    return true;
  }

  Future<bool> requestPermission() async {
    _permissionGranted = await _location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await _location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return false;
      }
    }
    return true;
  }

  Function get locationData {
    return _location.getLocation;
  }

  Stream<LocationData> get onLocationChanged {
    return _location.onLocationChanged;
  }

  LocationData get lastLocationData {
    return _locationData;
  }
}
