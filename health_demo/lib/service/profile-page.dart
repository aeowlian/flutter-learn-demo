// import 'dart:async';
// import 'dart:io';

// import 'package:flutter/foundation.dart';
// import 'package:flutter/gestures.dart';
// import 'package:flutter/material.dart';
// import 'package:image_picker/image_picker.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:dio/dio.dart';
// import 'package:location/location.dart';

// class ProfilePage extends StatefulWidget {
//   @override
//   _ProfilePageState createState() => _ProfilePageState();
// }

// class _ProfilePageState extends State<ProfilePage> {
//   bool useNetworkImage = true;
//   File image;
//   Location _location;
//   LatLng locationData;
//   CameraPosition position;

//   void initState() {
//     _location = new Location();

//     _location.getLocation().then((value) {
//       setState(() {
//         locationData = LatLng(
//           value.latitude,
//           value.longitude,
//         );
//         position = CameraPosition(
//           target: LatLng(
//             value.latitude,
//             value.longitude,
//           ),
//           zoom: 15,
//         );
//       });
//     });
//     // _location.onLocationChanged.listen((event) {
//     //   setState(() {
//     //     locationData = event.toString();
//     //   });
//     // });
//     super.initState();
//   }

//   Completer<GoogleMapController> _controller = Completer();

//   static final CameraPosition _kGooglePlex = CameraPosition(
//     target: LatLng(37.42796133580664, -122.085749655962),
//     zoom: 14.4746,
//   );

//   static final CameraPosition _kLake = CameraPosition(
//       bearing: 192.8334901395799,
//       target: LatLng(37.43296265331129, -122.08832357078792),
//       tilt: 59.440717697143555,
//       zoom: 19.151926040649414);

//   Widget getImage() {
//     if (useNetworkImage) {
//       return CircleAvatar(
//         radius: 80,
//         backgroundImage: NetworkImage(
//           'https://cdn.discordapp.com/attachments/514114858631954434/738730851025682492/Screenshot_2019-10-14-17-51-30-287_com.instagram.android.png',
//         ),
//       );
//     } else {
//       return CircleAvatar(radius: 80, backgroundImage: FileImage(image));
//     }
//   }

//   void askForImage(bool camera) {
//     final picker = ImagePicker()
//         .getImage(source: camera ? ImageSource.camera : ImageSource.gallery);
//     picker.then((value) {
//       setState(() {
//         image = File(value.path);
//         useNetworkImage = false;
//       });
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Center(
//       // child: CustomScrollView(
//       //   slivers: [
//       //     SliverAppBar(
//       //       expandedHeight: 200,
//       //       flexibleSpace: FlexibleSpaceBar(
//       //         background: Container(
//       //           color: Colors.blue,
//       //         ),
//       //       ),
//       //     )
//       //   ],
//       // ),
//       child: Column(
//         children: [
//           Stack(
//             children: [
//               Container(color: Colors.lightBlueAccent, height: 100),
//               Container(color: Colors.transparent, height: 100),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   Padding(
//                     padding: const EdgeInsets.only(top: 20, bottom: 35),
//                     child: Column(
//                       children: [
//                         getImage(),
//                         Text(
//                           'User Name',
//                           style: TextStyle(fontSize: 20),
//                         ),
//                         Row(
//                           children: [
//                             IconButton(
//                               icon: Icon(Icons.camera),
//                               onPressed: () {
//                                 askForImage(true);
//                               },
//                             ),
//                             IconButton(
//                               icon: Icon(Icons.folder),
//                               onPressed: () {
//                                 askForImage(false);
//                               },
//                             )
//                           ],
//                         )
//                       ],
//                     ),
//                   ),
//                 ],
//               ),
//             ],
//           ),
//           Container(
//             height: 400,
//             child: Card(
//               child: position != null
//                   ? GoogleMap(
//                       mapType: MapType.hybrid,
//                       initialCameraPosition: position,
//                       onMapCreated: (GoogleMapController controller) {
//                         _controller.complete(controller);
//                       },
//                       gestureRecognizers: Set()
//                         ..add(Factory<EagerGestureRecognizer>(
//                             () => EagerGestureRecognizer())),
//                       rotateGesturesEnabled: true,
//                       scrollGesturesEnabled: true,
//                       tiltGesturesEnabled: true,
//                       zoomGesturesEnabled: true,
//                       myLocationEnabled: true,
//                       myLocationButtonEnabled: true,
//                     )
//                   : Container(),
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
