package com.example.health_demo;

import android.os.Environment;

import androidx.annotation.NonNull;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.common.RationalNumber;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.GpsTagConstants;
import org.apache.commons.imaging.formats.tiff.taginfos.TagInfo;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputDirectory;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;

public class MainActivity extends FlutterActivity {
    private static final String CHANNEL = "locationMethodChannel";

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);
        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL)
            .setMethodCallHandler(
                (call, result) -> {
                    if(call.method.equals("addLocationToMetadata")) {
                        List argumentList = call.arguments();
                        result.success(addLocationToMetadata(
                            new File((String)argumentList.get(0)).getAbsoluteFile(),
                            (double)argumentList.get(1),
                            (double)argumentList.get(2),
                            (double)argumentList.get(3)
                        ));
                    }
                }
            );
    }

    public String addLocationToMetadata(File file, double altitude, double latitude, double longitude) {
        final ImageMetadata metadata;
        try {
            //read image metadata
            metadata = Imaging.getMetadata(file);
            final JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
            TiffImageMetadata tiffImageMetadata = jpegMetadata.getExif();
            TiffOutputSet tiffOutputSet = tiffImageMetadata.getOutputSet();

            //init rewriter
            ExifRewriter exifRewriter = new ExifRewriter();

            //create output file
            String fileName = file.getName().substring(0, file.getName().length()-4);
            System.out.println(fileName);
            File outputFile;
            File outputDir = getContext().getCacheDir(); // context being the Activity pointer
            outputFile = File.createTempFile("prefix", ".jpg", outputDir);
            System.out.println(outputFile.getCanonicalPath());

            //rewrite
            FileOutputStream outputStream;
            outputStream = new FileOutputStream(outputFile);
            tiffOutputSet.setGPSInDegrees(longitude, latitude);
            TiffOutputDirectory gpsDirectory = tiffOutputSet.getOrCreateGPSDirectory();
            gpsDirectory.removeField(GpsTagConstants.GPS_TAG_GPS_ALTITUDE_REF);
            gpsDirectory.add(GpsTagConstants.GPS_TAG_GPS_ALTITUDE, RationalNumber.valueOf(altitude));
            exifRewriter.updateExifMetadataLossless(file, outputStream, tiffOutputSet);
            File notTemporaryFile = new File(getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES), "newExifedImage.jpg");

            //copy temp file to another file in local storage
            InputStream in = new FileInputStream(outputFile);
            try {
                OutputStream out = new FileOutputStream(notTemporaryFile);
                try {
                    // Transfer bytes from in to out
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                } finally {
                    out.close();
                }
            } finally {
                in.close();
            }

            return notTemporaryFile.getAbsolutePath();
        } catch (ImageReadException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ImageWriteException e) {
            e.printStackTrace();
        }

        return "";
    }
}
