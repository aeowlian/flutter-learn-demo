import 'package:demo/cart.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:provider/provider.dart';

class CartPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<Cart>(builder: (context, cart, child) {
      return Container(
        height: MediaQuery.of(context).size.height - 60 - 80,
        child: ListView(
          padding: EdgeInsets.all(8),
          children: cart.getItems().map((element) {
            return Container(
              height: 110,
              child: Card(
                child: Row(
                  children: [
                    Image.network(
                      element.item.pictureUrl,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${element.item.name}',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                            textAlign: TextAlign.left,
                          ),
                          Text('Rp. ${element.item.price}'),
                        ],
                      ),
                    ),
                    Row(
                      // crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        IconButton(
                            icon: Icon(
                              Icons.remove_circle_outline,
                              color: Colors.red,
                            ),
                            onPressed: () {
                              cart.minus(element.item.id);
                            }),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 0),
                          child: Text('${element.amount}'),
                        ),
                        IconButton(
                            icon: Icon(
                              Icons.add_circle_outline,
                              color: Colors.green,
                            ),
                            onPressed: () {
                              cart.plus(element.item.id);
                            }),
                      ],
                    )
                  ],
                ),
              ),
            );
          }).toList(),
        ),
      );
    });
  }
}
