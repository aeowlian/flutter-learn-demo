import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter_dynamic_forms/flutter_dynamic_forms.dart';
import 'package:flutter_dynamic_forms_components/flutter_dynamic_forms_components.dart'
    hide Text, Container;
import 'package:dynamic_forms/dynamic_forms.dart';
import 'json-form-initializer.dart';

class JSONAddressForm extends StatefulWidget {
  List<Function> listeners;
  JSONAddressForm({this.listeners});

  String formString = json.encode({
    "@form": "address-form",
    "id": "address-form",
    "children": [
      {
        "@name": "textField",
        "id": "address-field",
        "label": "Address",
        "value": ""
      },
      {
        "@name": "textField",
        "id": "province-field",
        "label": "Province",
        "value": ""
      },
      {
        "@name": "textField",
        "id": "city-field",
        "label": "City",
        "value": "",
      },
      {
        "@name": "textField",
        "id": "postal-code-field",
        "label": "Postal Code",
        "value": ""
      }
    ]
  });

  @override
  _JSONAddressFormState createState() =>
      _JSONAddressFormState(formString: formString, listeners: listeners);
}

class _JSONAddressFormState extends State<JSONAddressForm> {
  String formString;
  JSONFormArtifacts formArtifacts;
  List<Function> listeners;

  _JSONAddressFormState({this.formString, this.listeners});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    formArtifacts = JSONFormInitializer.getArtifacts(
      dispatcher: _onFormElementEvent,
      formString: formString,
    );
  }

  void _onFormElementEvent(FormElementEvent event) {
    if (event is ChangeValueEvent) {
      print(event.elementId);
      print(event.propertyName);
      print(event.value);
      formArtifacts.formManager.changeValue(
        value: event.value,
        elementId: event.elementId,
        propertyName: event.propertyName,
      );
      for (var listener in listeners) {
        listener(event);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: formArtifacts.form == null
          ? CircularProgressIndicator()
          : formArtifacts.formRenderService.render(formArtifacts.form, context),
    ));
  }
}
