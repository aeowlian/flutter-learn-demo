import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

/// Build form widget by extending this class and call super constructor
/// For example:
/// ```
/// ChildClass(a) : super(a)
/// ```
///
/// Callback function [onChanged] will return values of all form fields
/// in the form of a [Map<String, dynamic>].
///
/// Additional validators can be accepted in constructor
/// ```
/// SomeForm({
///   Function(Map<String, dynamic>) onChanged,
///   fieldOneValidator,
///   fieldTwoValidator,
///   fieldThreeValidator,
/// }) : super(onChanged: onChanged, validators: {
///         fieldOne: fieldOneValidator ?? [],
///         fieldTwo: fieldTwoValidator ?? [],
///         fieldThree: fieldThreeValidator ?? [],
///       });
/// ```
///
/// These additional validator has to be manually implemented
/// into each form widget used e.g.
/// ```
/// FormBuilderTextField(
///   attribute: 'fieldOne',
///   decoration: InputDecoration(
///     labelText: 'Field One',
///   ),
///   validators: [FormBuilderValidators.required(), ...validators[fieldOne]],
/// ),
/// ```
abstract class FormWidget extends StatelessWidget {
  final Function(GlobalKey<FormBuilderState> values) onChanged;
  final GlobalKey<FormBuilderState> _formState = GlobalKey<FormBuilderState>();

  /// List of functions that can be added to each form validations array
  final Map<String, List<Function(dynamic)>> validators;

  @required
  FormWidget({@required this.onChanged, this.validators});

  String getState(String key) {
    return _formState.currentState.fields[key].currentState.value;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        child: FormBuilder(
          onChanged: (val) {
            this.onChanged(_formState);
          },
          key: _formState,
          autovalidate: true,
          child: Column(children: buildForm()),
        ),
      ),
    );
  }

  List<Widget> buildForm();
}
