import 'package:demo/cart.dart';
import 'package:demo/model/item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:carousel_slider/carousel_slider.dart';

class HomePage extends StatelessWidget {
  final List<Item> itemList = [
    Item(1, 500, 'Item 1', 'https://picsum.photos/250'),
    Item(2, 500, 'Item 2', 'https://picsum.photos/250'),
    Item(3, 500, 'Item 3', 'https://picsum.photos/250'),
    // Item(4, 500, 'Item 4', 'https://picsum.photos/250'),
    // Item(5, 500, 'Item 5', 'https://picsum.photos/250'),
    // Item(6, 500, 'Item 6', 'https://picsum.photos/250'),
    // Item(7, 500, 'Item 7', 'https://picsum.photos/250'),
    // Item(8, 500, 'Item 8', 'https://picsum.photos/250'),
    // Item(9, 500, 'Item 9', 'https://picsum.photos/250'),
    // Item(10, 500, 'Item 10', 'https://picsum.photos/250'),
    // Item(11, 500, 'Item 11', 'https://picsum.photos/250'),
    // Item(12, 500, 'Item 12', 'https://picsum.photos/250'),
    // Item(13, 500, 'Item 13', 'https://picsum.photos/250'),
    // Item(14, 500, 'Item 14', 'https://picsum.photos/250'),
    // Item(15, 500, 'Item 15', 'https://picsum.photos/250'),
  ];

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () {
        print('refreshed');
        return;
      },
      child: Container(
        height: MediaQuery.of(context).size.height - 140,
        child: ListView(
          children: <Widget>[
            // Carousel
            CarouselSlider(
              options: CarouselOptions(height: 250.0),
              items: [
                'https://picsum.photos/400/500',
                'https://picsum.photos/400/500',
                'https://picsum.photos/400/500',
                'https://picsum.photos/400/500',
                'https://picsum.photos/400/500',
                'https://picsum.photos/400/500',
                'https://picsum.photos/400/500',
                'https://picsum.photos/400/500',
                'https://picsum.photos/400/500',
              ].map((image) {
                return Builder(
                  builder: (BuildContext context) {
                    return Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.symmetric(horizontal: 5.0),
                      // decoration: BoxDecoration(color: Colors.amber),
                      color: Colors.amber,
                      child: InkWell(onTap: null, child: Image.network(image)),
                    );
                  },
                );
              }).toList(),
            ),
            // Item Grid
            Consumer<Cart>(builder: (context, cart, child) {
              return Column(
                children: itemList.map((element) {
                  return Container(
                    height: 100,
                    child: Card(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Image.network(element.pictureUrl),
                          Expanded(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  children: [
                                    Text(element.name),
                                    Text('Rp. ${element.price}')
                                  ],
                                ),
                                RawMaterialButton(
                                  onPressed: () {
                                    cart.add(element, 1);
                                  },
                                  elevation: 2.0,
                                  fillColor: Colors.white,
                                  child: Icon(
                                    Icons.add_shopping_cart,
                                    size: 35.0,
                                  ),
                                  padding: EdgeInsets.all(15.0),
                                  shape: CircleBorder(),
                                ),
                                Builder(builder: (BuildContext ctx) {
                                  return IconButton(
                                    icon: Icon(Icons.add_shopping_cart),
                                    onPressed: () {
                                      cart.add(element, 1);
                                      Scaffold.of(ctx).showSnackBar(
                                        SnackBar(
                                          content: Text(
                                            'Added ${element.name} to cart!',
                                          ),
                                          duration: Duration(seconds: 2),
                                        ),
                                      );
                                    },
                                  );
                                })
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }).toList(),
              );
            }),
          ],
        ),
      ),
    );
  }
}
