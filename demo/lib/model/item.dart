class Item {
  final int id;
  final double price;
  final String name;
  final String pictureUrl;

  Item(this.id, this.price, this.name, this.pictureUrl);
}
