import 'package:flutter/foundation.dart';

class Setting {
  String title;
  String subtitle;

  Setting({@required this.title, this.subtitle});
}
