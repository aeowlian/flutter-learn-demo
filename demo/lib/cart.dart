import 'package:flutter/material.dart';

import 'package:flutter/material.dart';
import './model/item.dart';

class Cart with ChangeNotifier {
  List<CartItem> _itemInCart = [
    CartItem(Item(1, 1000, 'test item', 'https://picsum.photos/250'), 1)
  ];

  void add(Item item, int amount) {
    findItem(item.id, (responseItem, position) {
      if (position == null)
        _itemInCart.add(CartItem(item, amount));
      else
        plus(item.id);
      print(_itemInCart.toString());
    });
    notifyListeners();
  }

  void remove(int index) {
    findItem(index, (_, i) {
      _itemInCart.removeAt(i);
    });
  }

  void plus(int itemId) {
    findItem(itemId, (item, _) {
      item.amount += 1;
    });
  }

  void minus(int itemId) {
    findItem(itemId, (item, _) {
      if (item.amount == 1) remove(itemId);
      item.amount -= 1;
    });
  }

  void findItem(int itemId, void doToItem(cartItem, position)) {
    CartItem cartItem;
    for (var i = 0; i < _itemInCart.length; i++) {
      cartItem = _itemInCart[i];
      if (cartItem.item.id == itemId) {
        print(cartItem);
        doToItem(cartItem, i);
        notifyListeners();
        return;
      }
    }
    doToItem(null, null);
  }

  List<CartItem> getItems() {
    return [..._itemInCart];
  }
}

class CartItem {
  Item item;
  int amount;

  CartItem(this.item, this.amount);
}
