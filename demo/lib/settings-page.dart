import 'package:demo/model/setting.dart';
import 'package:demo/settings-component.dart';
import 'package:flutter/material.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  List<Setting> settingList = [
    Setting(
      title: 'General',
      subtitle: 'General settings',
    ),
    Setting(
      title: 'Profile',
      subtitle: 'Profiles are visible to other users',
    ),
    Setting(
      title: 'Account',
      subtitle: 'Upgrade account, logout, set password, etc',
    ),
    Setting(
      title: 'Notification',
      subtitle: 'Are we too noisy?',
    ),
    Setting(
      title: 'Payment',
      subtitle: 'Set your favourite payment method',
    ),
    Setting(
      title: 'Security',
      subtitle: 'Account security, two-factor authentication, safe mode',
    ),
    Setting(
      title: 'About',
    ),
    Setting(
      title: 'Terms & Conditions',
    ),
    Setting(
      title: 'Privacy Policy',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text('Setting'),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height - 80,
        child: ListView(
          children: settingList.map((element) {
            return SettingItem(
              setting: element,
              isLastItem: settingList.last.title == element.title,
            );
          }).toList(),
        ),
      ),
    );
  }
}
