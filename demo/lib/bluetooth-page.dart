import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

class BluetoothPage extends StatefulWidget {
  @override
  _BluetoothPageState createState() => _BluetoothPageState();
}

class _BluetoothPageState extends State<BluetoothPage> {
  FlutterBlue flutterBlue = FlutterBlue.instance;
  BluetoothConnection connection;
  // List<BluetoothDiscoveryResult> devices = [];
  List<ScanResult> devices = [];

  void scan() {
    FlutterBlue.instance.startScan(timeout: Duration(seconds: 4));
    FlutterBlue.instance.scanResults.listen((results) {
      print(results);
      print('results');
      for (ScanResult r in results) {
        setState(() {
          devices.add(r);
        });
        print('${r.device.name} found! rssi: ${r.rssi}');
      }
    });
    // FlutterBluetoothSerial.instance.startDiscovery().listen((event) {
    //   setState(() {
    //     devices.add(event);
    //     print(event.device.name);
    //   });
    // });
  }

  void stop() {
    FlutterBlue.instance.stopScan();
  }

  void tryConnection(ScanResult device) async {
    try {
      device.device.discoverServices().then((value) {
        for (var item in value) {
          print(item);
        }
      });
      // BluetoothBondState status =
      //     await FlutterBluetoothSerial.instance.getBondStateForAddress(address);
      // bool connected = await FlutterBluetoothSerial.instance.isEnabled;
      // if (status.isBonded) {
      //   print('Device is paired!');
      //   SnackBar snackBar = SnackBar(content: Text('Device is paired'));
      //   Scaffold.of(context).showSnackBar(snackBar);
      // } else {
      //   FlutterBluetoothSerial.instance
      //       .bondDeviceAtAddress(address)
      //       .then((value) => print(value));
      // }
      // try {
      //   BluetoothConnection.toAddress(address).then((connection) {
      //     this.connection = connection;
      //     // connection.
      //   });
      // } catch (e) {}
      // if (connection != null && connection.isConnected) {
      //   print('sending');
      //   try {
      //     connection.output.add(ascii.encode('source: datadatadata'));
      //   } catch (e) {}
      //   print('sent');
      // }
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        child: RefreshIndicator(
          onRefresh: () async {
            return await FlutterBlue.instance
                .startScan(timeout: Duration(seconds: 4));
          },
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                StreamBuilder<List<ScanResult>>(
                  stream: FlutterBlue.instance.scanResults,
                  initialData: [],
                  builder: (BuildContext context, snapshot) {
                    print(snapshot);
                    return Column(
                        children: snapshot.data.map(
                      (result) {
                        return StreamBuilder<BluetoothDeviceState>(
                          stream: result.device.state,
                          initialData: BluetoothDeviceState.disconnected,
                          builder: (context, snapshot) {
                            return Card(
                              child: Row(
                                children: [
                                  (snapshot.data ==
                                          BluetoothDeviceState.connected)
                                      ? Icon(Icons.bluetooth_connected)
                                      : Icon(Icons.bluetooth_disabled),
                                  Column(
                                    children: [
                                      Text(result.device.name),
                                      Text(result.advertisementData.serviceData
                                          .toString())
                                    ],
                                  )
                                ],
                              ),
                            );
                          },
                        );
                      },
                    ).toList());
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    RaisedButton(
                      onPressed: () {
                        setState(
                          () {
                            FlutterBlue.instance
                                .startScan(timeout: Duration(seconds: 4));
                          },
                        );
                      },
                      child: Text('Scan'),
                    ),
                    RaisedButton(
                      onPressed: () {
                        setState(
                          () {
                            stop();
                          },
                        );
                      },
                      child: Text('Stop'),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
