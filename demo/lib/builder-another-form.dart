import 'package:demo/form-widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class AnotherForm extends FormWidget {
  static final province = 'province';
  static final city = 'city';
  static final address = 'address';
  static final postalCode = 'postalCode';

  /// A form that accepts address fields such as
  /// province, city, address, and postal code.
  ///
  /// * [onChanged] accept a callback function that gives
  /// values of the fields in the function's arguments.
  ///
  /// Additional validators accept a list of additional validator
  /// functions for the respective fields. Use hand-made validator or
  /// built-in validator in FormBuilderValidator.
  /// * [provinceValidator]
  /// * [cityValidator]
  /// * [addressValidator]
  /// * [postalCodeValidator]
  AnotherForm({
    @required Function(GlobalKey<FormBuilderState> key) onChanged,
    provinceValidator,
    cityValidator,
    addressValidator,
    postalCodeValidator,
  }) : super(onChanged: onChanged, validators: {
          province: provinceValidator ?? [],
          city: cityValidator ?? [],
          address: addressValidator ?? [],
          postalCode: postalCodeValidator ?? [],
        });

  @override
  List<Widget> buildForm() {
    return [
      FormBuilderTextField(
        attribute: 'province',
        decoration: InputDecoration(
          labelText: 'Province',
        ),
        validators: [FormBuilderValidators.required(), ...validators[province]],
      ),
      FormBuilderTextField(
        attribute: 'city',
        decoration: InputDecoration(
          labelText: 'City',
        ),
        validators: [FormBuilderValidators.required(), ...validators[city]],
      ),
      //   ],
      // ),
      FormBuilderTextField(
        attribute: 'address',
        decoration: InputDecoration(
          labelText: 'Address',
        ),
        validators: [FormBuilderValidators.required(), ...validators[address]],
      ),
      FormBuilderTextField(
        attribute: 'postalCode',
        decoration: InputDecoration(
          labelText: 'Postal Code',
        ),
        validators: [
          FormBuilderValidators.required(),
          ...validators[postalCode]
        ],
      ),
      // Divider(
      //   thickness: 1,
      //   color: Colors.red,
      // ),
      // Text(
      //   'This is an error text',
      //   style: TextStyle(color: Colors.red, fontSize: 12),
      // )
    ];
  }
}
