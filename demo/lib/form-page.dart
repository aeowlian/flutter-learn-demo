import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dynamic_forms/flutter_dynamic_forms.dart';
import 'package:flutter_dynamic_forms_components/flutter_dynamic_forms_components.dart'
    hide Text;
import 'package:dynamic_forms/dynamic_forms.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class FormPage extends StatefulWidget {
  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  FormRenderService _formRenderService;
  FormManager _formManager;
  FormElement _form;
  bool rand = true;
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  Future _buildForm() async {
    var parsers = getDefaultParserList(); //Add your custom parsers
    var renderers = getReactiveRenderers(); //Add your custom renderers
    _formRenderService = FormRenderService(
      renderers: renderers,
      dispatcher: _onFormElementEvent,
    );

    //Use JsonFormParserService for the JSON format
    var formManagerBuilder = FormManagerBuilder(JsonFormParserService(parsers));
    var content = json.encode({
      "@name": "form",
      "id": "form1",
      "children": [
        {
          "@name": "formGroup",
          "id": "formGroup1",
          "name": "Form section 1",
          "children": [
            {
              "@name": "textField",
              "id": "firstName",
              "label": "Enter your first name",
              "value": "John",
              "validations": [
                {
                  "@name": "validation",
                  "message": "Name is too long",
                  "isValid": {"expression": "length(@firstName) < 15"}
                },
                {
                  "@name": "requiredValidation",
                  "message": "First name is required"
                }
              ]
            },
            {
              "@name": "textField",
              "id": "lastName",
              "label": "Enter your last name",
              "validations": [
                {
                  "@name": "requiredValidation",
                  "message": "Last name is required"
                }
              ]
            },
            {
              "@name": "label",
              "id": "fullNameLabel",
              "value": {
                "expression":
                    "@firstName + (length(@firstName) > 0 && length(@lastName) > 0 ? \" \": \"\") + @lastName"
              }
            },
            {
              "@name": "label",
              "value": {"expression": "\"Welcome \" + @fullNameLabel + \"!\""},
              "isVisible": {
                "expression":
                    "!@hideWelcomeCheckBox && length(@fullNameLabel) > 0"
              }
            },
            {
              "@name": "checkBox",
              "id": "hideWelcomeCheckBox",
              "value": "false",
              "label": getBool() ? "hide" : "not hide"
            },
            {
              "@name": "date",
              "id": "form_date",
              "label": "Form Date",
              "firstDate": "2010-02-27",
              "lastDate": "2030-02-27",
              "format": "yyyy-MM-dd",
              "initialDate": "2019-10-20"
            },
            {
              "@name": "date",
              "id": "form_date2",
              "label": "Form Date",
              "firstDate": "2010-02-27",
              "lastDate": "2030-02-27",
              "format": "yyyy-MM-dd",
              "initialDate": "2019-10-20"
            }
          ]
        }
      ]
    }); //Load your form
    _formManager = formManagerBuilder.build(content);
    setState(() {
      _form = _formManager.form;
    });
  }

  bool getBool() {
    rand = !rand;
    return rand;
  }

  String submit() {
    print(_fbKey.currentState.fields['accept_terms'].currentState.value);
    List<FormItemValue> data = _formManager.getFormData();
    return data.removeLast().name;
  }

  void _onFormElementEvent(FormElementEvent event) {
    if (event is ChangeValueEvent) {
      print(event.elementId);
      print(event.propertyName);
      print(event.value);
      _formManager.changeValue(
        value: event.value,
        elementId: event.elementId,
        propertyName: event.propertyName,
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _buildForm();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: SingleChildScrollView(
          child: _form == null
              ? CircularProgressIndicator()
              : _formRenderService.render(_form, context),
        ),
      ),
    );
  }
}
