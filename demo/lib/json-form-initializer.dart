import 'package:flutter_dynamic_forms/flutter_dynamic_forms.dart';
import 'package:flutter_dynamic_forms_components/flutter_dynamic_forms_components.dart'
    hide Text, Container;
import 'package:dynamic_forms/dynamic_forms.dart';

class JSONFormInitializer {
  static JSONFormArtifacts getArtifacts(
      {Function dispatcher, String formString}) {
    FormRenderService _formRenderService;
    FormManager _formManager;
    FormElement _form;

    var parsers = getDefaultParserList(); //Add your custom parsers
    var renderers = getReactiveRenderers(); //Add your custom renderers
    _formRenderService = FormRenderService(
      renderers: renderers,
      dispatcher: dispatcher,
    );

    var formManagerBuilder = FormManagerBuilder(JsonFormParserService(parsers));
    _formManager = formManagerBuilder.build(formString);
    _form = _formManager.form;

    return JSONFormArtifacts(
      formRenderService: _formRenderService,
      formManager: _formManager,
      form: _form,
    );
  }
}

class JSONFormArtifacts {
  FormRenderService formRenderService;
  FormManager formManager;
  FormElement form;

  JSONFormArtifacts({this.form, this.formManager, this.formRenderService});
}
