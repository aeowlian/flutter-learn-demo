import 'package:demo/model/setting.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class SettingItem extends StatefulWidget {
  final Setting setting;
  final bool isLastItem;

  SettingItem({@required this.setting, this.isLastItem});

  @override
  _SettingItemState createState() => _SettingItemState();
}

class _SettingItemState extends State<SettingItem> {
  bool isLoading = false;

  void loading() {
    setState(() {
      isLoading = !isLoading;
    });
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        loading();
      },
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '${widget.setting.title}',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 19),
                          textAlign: TextAlign.left,
                        ),
                        widget.setting.subtitle != null
                            ? Text(
                                '${widget.setting.subtitle}',
                                style: TextStyle(color: Colors.grey),
                              )
                            : Container(height: 0, width: 0),
                      ],
                    ),
                  ),
                  isLoading
                      ? SpinKitFadingCircle(
                          color: Colors.blueAccent,
                          size: 50.0,
                        )
                      : Icon(Icons.arrow_forward_ios)
                ],
              ),
            ),
            widget.isLastItem
                ? Container(height: 0, width: 0)
                : Divider(
                    color: Colors.blueGrey,
                    height: 10,
                    thickness: 1,
                    indent: 20,
                    endIndent: 20,
                  )
          ],
        ),
      ),
    );
  }
}
