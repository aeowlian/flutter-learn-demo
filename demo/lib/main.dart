import 'package:demo/bluetooth-page.dart';
import 'package:demo/bluetooth-page2.dart';
import 'package:demo/bluetooth-page3.dart';
import 'package:demo/builder-person-form.dart';
import 'package:demo/cart.dart';
import 'package:demo/home-page.dart';
import 'package:demo/cart-page.dart';
import 'package:demo/sensor-page.dart';
import 'package:demo/profile-page.dart';
import 'package:demo/model/item.dart';
import 'package:demo/settings-page.dart';
import 'package:demo/form-page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:location_permissions/location_permissions.dart';
import 'package:provider/provider.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:flutter_ble_lib/flutter_ble_lib.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'TestApp'),
      routes: {
        '/setting': (context) => SettingsPage(),
        '/light': (context) => SensorPage(),
        '/bluetooth': (context) => BluetoothPage3(),
        '/form': (context) => PersonForm(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //inPage stores current page status
  //home, cart, profile
  String inPage = 'home';
  BleManager bleManager;
  Map<String, ScanResult> peripherals = {};

  _goToPage(String page) {
    print(page);
    setState(() {
      inPage = page;
    });
  }

  _init() async {
    PermissionStatus permission =
        await LocationPermissions().checkPermissionStatus();
    if (permission != PermissionStatus.granted) {
      LocationPermissions().requestPermissions();
    }

    bleManager = BleManager();
    await bleManager.createClient();

    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();
// initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    var initializationSettingsAndroid =
        AndroidInitializationSettings("@mipmap/ic_launcher");
    var initializationSettingsIOS = IOSInitializationSettings();
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'channelid', 'your channel name', 'your channel description',
        importance: Importance.Default,
        priority: Priority.Default,
        ticker: 'ticker');
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);

    final int alarmID = 0;
    bleManager.startPeripheralScan();
    await AndroidAlarmManager.initialize();
    AndroidAlarmManager.oneShot(Duration(seconds: 7), 2, () {
      bleManager.stopPeripheralScan();
    }, exact: true);
    await AndroidAlarmManager.periodic(const Duration(seconds: 10), alarmID,
        () {
      bleManager
          .startPeripheralScan(allowDuplicates: false)
          .listen((scanResult) {
        // print(scanResult.advertisementData);
        // print(scanResult);
        // setState(() {});
        // peripherals.addAll({scanResult.peripheral.identifier: scanResult});
        setState(() {
          peripherals.putIfAbsent(scanResult.peripheral.identifier, () {
            flutterLocalNotificationsPlugin.show(
              0,
              'Device is nearby!',
              'Body',
              platformChannelSpecifics,
            );
            print('notification shown');
            return scanResult;
          });
        });
      });
      AndroidAlarmManager.oneShot(Duration(seconds: 7), 2, () {
        bleManager.stopPeripheralScan();
      }, exact: true);
    }, exact: true);
  }

  @override
  void initState() {
    // TODO: implement initState
    _init();
    super.initState();
  }

  Future selectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
    }
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => BluetoothPage3()),
    );
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      // persistentFooterButtons: <Widget>[
      //   Row(
      //     children: <Widget>[
      //       IconButton(icon: Icon(Icons.person), onPressed: null),
      //       IconButton(icon: Icon(Icons.person), onPressed: null),
      //     ],
      //   )
      // ],
      bottomNavigationBar: Container(
        color: Colors.transparent,
        height: 60,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: EdgeInsets.all(5),
              child: IconButton(
                color: inPage == 'home' ? Colors.amber : Colors.black,
                splashColor: Colors.amberAccent,
                highlightColor: Colors.pink,
                icon: Icon(Icons.home),
                onPressed: () {
                  _goToPage('home');
                },
                tooltip: 'Home',
              ),
            ),
            Padding(
              padding: EdgeInsets.all(5),
              child: IconButton(
                color: inPage == 'cart' ? Colors.amber : Colors.black,
                splashColor: Colors.amberAccent,
                highlightColor: Colors.pink,
                icon: Icon(Icons.shopping_cart),
                onPressed: () {
                  _goToPage('cart');
                },
                tooltip: 'Cart',
              ),
            ),
            Padding(
              padding: EdgeInsets.all(5),
              child: IconButton(
                color: inPage == 'profile' ? Colors.amber : Colors.black,
                splashColor: Colors.amberAccent,
                highlightColor: Colors.pink,
                icon: Icon(Icons.person),
                onPressed: () {
                  _goToPage('profile');
                },
                tooltip: 'Profile',
              ),
            ),
          ],
        ),
      ),
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
        actions: [
          IconButton(
            icon: Icon(
              Icons.settings,
            ),
            onPressed: () {
              Navigator.pushNamed(context, '/setting');
            },
          ),
          IconButton(
            icon: Icon(
              Icons.lightbulb_outline,
            ),
            onPressed: () {
              Navigator.pushNamed(context, '/light');
            },
          ),
          IconButton(
            icon: Icon(
              Icons.bluetooth,
            ),
            onPressed: () {
              Navigator.pushNamed(context, '/bluetooth');
            },
          ),
          IconButton(
            icon: Icon(
              Icons.computer,
            ),
            onPressed: () {
              Navigator.pushNamed(context, '/form');
            },
          ),
        ],
      ),
      body: ChangeNotifierProvider(
        create: (context) => Cart(),
        child: SingleChildScrollView(
          child: inPage == 'home'
              ? HomePage()
              : inPage == 'cart' ? CartPage() : ProfilePage(),
        ),
      ),

      // floatingActionButton: FloatingActionButton(
      //   onPressed: _incrementCounter,
      //   tooltip: 'Increment',
      //   child: Icon(Icons.add),
      // ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
