// import 'package:demo/home.dart';
// import 'package:demo/model/transaction.dart';
// import 'package:flutter/material.dart';
// import 'home.dart';

// class MyList extends StatelessWidget {
//   final List<Transaction> transactions;

//   MyList(this.transactions);

//   void movePage(BuildContext context) {
//     Navigator.of(context).push(
//       MaterialPageRoute(builder: (_) {
//         return DetailPage();
//       }),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Column(
//         children: transactions.map((Transaction item) {
//           print(item.price.toString());
//           return InkWell(
//             onTap: () => movePage(context),
//             splashColor: Theme.of(context).primaryColor,
//             borderRadius: BorderRadius.circular(15),
//             child: Card(
//               child: Row(
//                 children: [
//                   Text('Rp. ${item.price}'),
//                   Column(
//                     children: [
//                       Text('${item.name}'),
//                       Text(
//                         '${item.date.toString()}',
//                       )
//                     ],
//                   ),
//                 ],
//               ),
//             ),
//           );
//         }).toList(),
//       ),
//     );
//   }
// }
