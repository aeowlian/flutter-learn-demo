import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class AddressForm extends StatelessWidget {
  Function(Map<String, dynamic>) onChanged;
  List<Function(dynamic)> provinceValidator;
  List<Function(dynamic)> cityValidator;
  List<Function(dynamic)> addressValidator;
  List<Function(dynamic)> postalCodeValidator;
  GlobalKey<FormBuilderState> _formState = GlobalKey<FormBuilderState>();

  static final province = 'province';
  static final city = 'city';
  static final address = 'address';
  static final postalCode = 'postalCode';

  AddressForm({
    /// Give province, city, address, postalCode as onChanged arguments
    @required this.onChanged,
    provinceValidator,
    cityValidator,
    addressValidator,
    postalCodeValidator,
  }) {
    this.provinceValidator = provinceValidator ?? [];
    this.cityValidator = cityValidator ?? [];
    this.addressValidator = addressValidator ?? [];
    this.postalCodeValidator = postalCodeValidator ?? [];
  }

  String getState(String key) {
    return _formState.currentState.fields[key].currentState.value;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.all(8),
        child: FormBuilder(
          key: _formState,
          autovalidate: true,
          onChanged: (map) {
            onChanged(map);
          },
          child: Column(
            children: [
              // Row(
              //   children: [
              FormBuilderTextField(
                attribute: 'province',
                decoration: InputDecoration(
                  labelText: 'Province',
                ),
                validators: [
                  FormBuilderValidators.required(),
                  ...provinceValidator
                ],
              ),
              FormBuilderTextField(
                attribute: 'city',
                decoration: InputDecoration(
                  labelText: 'City',
                ),
                validators: [
                  FormBuilderValidators.required(),
                  ...cityValidator
                ],
              ),
              //   ],
              // ),
              FormBuilderTextField(
                attribute: 'address',
                decoration: InputDecoration(
                  labelText: 'Address',
                ),
                validators: [
                  FormBuilderValidators.required(),
                  ...addressValidator
                ],
              ),
              FormBuilderTextField(
                attribute: 'postalCode',
                decoration: InputDecoration(
                  labelText: 'Postal Code',
                ),
                validators: [
                  FormBuilderValidators.required(),
                  ...postalCodeValidator
                ],
              ),
              Divider(
                thickness: 1,
                color: Colors.red,
              ),
              Text(
                'This is an error text',
                style: TextStyle(color: Colors.red, fontSize: 12),
              )
            ],
          ),
        ),
      ),
    );
  }
}
