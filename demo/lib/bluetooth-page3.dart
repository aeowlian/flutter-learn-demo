import 'package:flutter/material.dart';
import 'package:flutter_ble_lib/flutter_ble_lib.dart';
import 'package:location_permissions/location_permissions.dart';

class BluetoothPage3 extends StatefulWidget {
  @override
  _BluetoothPage3State createState() => _BluetoothPage3State();
}

class _BluetoothPage3State extends State<BluetoothPage3> {
  BleManager bleManager;
  Map<String, ScanResult> peripherals = {};
  Peripheral connectedPeripheral;
  int oxyLevel, ppm;
  int systol, diastol, pulse;
  int glucoseLevel;
  final bpmUUIDS = {
    '00001800-0000-1000-8000-00805f9b34fb': [
      '00002a00-0000-1000-8000-00805f9b34fb',
      '00002a01-0000-1000-8000-00805f9b34fb',
      '00002a02-0000-1000-8000-00805f9b34fb',
      '00002a03-0000-1000-8000-00805f9b34fb',
      '00002a04-0000-1000-8000-00805f9b34fb',
    ],
    '0000180a-0000-1000-8000-00805f9b34fb': [
      '00002a23-0000-1000-8000-00805f9b34fb',
      '00002a24-0000-1000-8000-00805f9b34fb',
      '00002a25-0000-1000-8000-00805f9b34fb',
      '00002a26-0000-1000-8000-00805f9b34fb',
      '00002a27-0000-1000-8000-00805f9b34fb',
      '00002a28-0000-1000-8000-00805f9b34fb',
      '00002a29-0000-1000-8000-00805f9b34fb',
      '00002a2a-0000-1000-8000-00805f9b34fb',
      '00002a50-0000-1000-8000-00805f9b34fb',
    ],
    '0000fff0-0000-1000-8000-00805f9b34fb': [
      '0000fff1-0000-1000-8000-00805f9b34fb',
      '0000fff2-0000-1000-8000-00805f9b34fb',
      '0000fff3-0000-1000-8000-00805f9b34fb',
      '0000fff4-0000-1000-8000-00805f9b34fb',
      '0000fff5-0000-1000-8000-00805f9b34fb',
      '0000fff6-0000-1000-8000-00805f9b34fb',
    ],
    '0000180f-0000-1000-8000-00805f9b34fb': [
      '00002a19-0000-1000-8000-00805f9b34fb',
    ],
  };

  final oxymeterUUIDs = {
    '00001800-0000-1000-8000-00805f9b34fb': [
      '00002a00-0000-1000-8000-00805f9b34fb',
      '00002a01-0000-1000-8000-00805f9b34fb',
      '00002a02-0000-1000-8000-00805f9b34fb',
      '00002a03-0000-1000-8000-00805f9b34fb',
      '00002a04-0000-1000-8000-00805f9b34fb',
    ],
    '00001801-0000-1000-8000-00805f9b34fb': [
      '00002a05-0000-1000-8000-00805f9b34fb',
    ],
    '0000180a-0000-1000-8000-00805f9b34fb': [
      '00002a23-0000-1000-8000-00805f9b34fb',
      '00002a24-0000-1000-8000-00805f9b34fb',
      '00002a25-0000-1000-8000-00805f9b34fb',
      '00002a26-0000-1000-8000-00805f9b34fb',
      '00002a27-0000-1000-8000-00805f9b34fb',
      '00002a28-0000-1000-8000-00805f9b34fb',
      '00002a29-0000-1000-8000-00805f9b34fb',
      '00002a2a-0000-1000-8000-00805f9b34fb',
      '00002a50-0000-1000-8000-00805f9b34fb',
    ],
    'cdeacb80-5235-4c07-8846-93a37ee6b86d': [
      'cdeacb84-5235-4c07-8846-93a37ee6b86d',
      'cdeacb83-5235-4c07-8846-93a37ee6b86d',
      'cdeacb82-5235-4c07-8846-93a37ee6b86d',
      'cdeacb81-5235-4c07-8846-93a37ee6b86d',
      'cdeacb85-5235-4c07-8846-93a37ee6b86d',
    ],
    'f000ffc0-0451-4000-b000-000000000000': [
      'f000ffc1-0451-4000-b000-000000000000',
      'f000ffc2-0451-4000-b000-000000000000',
    ]
  };

  final glucose = {
    '0000fff0-0000-1000-8000-00805f9b34fb': [
      '0000fff1-0000-1000-8000-00805f9b34fb',
      '0000fff4-0000-1000-8000-00805f9b34fb',
      '0000fff3-0000-1000-8000-00805f9b34fb',
      '0000fff2-0000-1000-8000-00805f9b34fb',
      '0000fff5-0000-1000-8000-00805f9b34fb',
      '0000ff01-0000-1000-8000-00805f9b34fb',
    ],
  };

  final amazfit = {
    '0000fed1-0000-3512-2118-0009af100700': [
      '0000fed2-0000-3512-2118-0009af100700',
      '0000fedd-0000-1000-8000-00805f9b34fb',
      '0000fede-0000-1000-8000-00805f9b34fb',
    ]
  };

  void _init() async {
    bleManager = BleManager();
    await bleManager.createClient();
  }

  void ensurePermission() async {
    PermissionStatus permission =
        await LocationPermissions().checkPermissionStatus();
    if (permission != PermissionStatus.granted) {
      LocationPermissions().requestPermissions();
    }
  }

  @override
  void initState() {
    ensurePermission();
    _init();
    super.initState();
  }

  void _startScan() {
    bleManager.startPeripheralScan(allowDuplicates: false).listen((scanResult) {
      // print(scanResult.advertisementData);
      // print(scanResult);
      // setState(() {});
      // peripherals.addAll({scanResult.peripheral.identifier: scanResult});
      setState(() {
        peripherals.putIfAbsent(scanResult.peripheral.identifier, () {
          return scanResult;
        });
      });
    });
  }

  void _stopScan() {
    bleManager.stopPeripheralScan();
  }

  bool _isElementConnected(ScanResult element) {
    element.peripheral.isConnected().then((value) {
      return value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        child: Center(
          child: Column(
            children: [
              ...(peripherals.values.map((element) {
                return InkWell(
                  onTap: () {
                    element.peripheral.connect().then((_) {
                      setState(() {
                        _stopScan();
                        connectedPeripheral = element.peripheral;

                        connectedPeripheral
                            .discoverAllServicesAndCharacteristics()
                            .then((_) {
                          connectedPeripheral.services().then((services) {
                            services.forEach((service) {
                              // print(service.uuid);
                              service.characteristics().then((characteristics) {
                                characteristics.forEach((characteristic) {
                                  characteristic.monitor().listen((event) {});
                                  characteristic.read().then((value) {});
                                  print('============================');
                                  print(characteristic.service.uuid);
                                  print(characteristic.uuid);
                                  print('readable?');
                                  print(characteristic.isReadable);
                                  print('notifiable?');
                                  print(characteristic.isReadable);
                                  // print(characteristic.isNotifiable);
                                  // if (characteristic.isNotifiable) {
                                  //   connectedPeripheral
                                  //       .monitorCharacteristic(
                                  //           characteristic.service.uuid,
                                  //           characteristic.uuid)
                                  //       .listen((event) {
                                  //     print(event.service.uuid);
                                  //     print(event.uuid);
                                  //     print(event.value);
                                  //     // if (event.value.length == 4) {
                                  //     //   setState(() {
                                  //     //     ppm = event.value[1];
                                  //     //     oxyLevel = event.value[2];
                                  //     //   });
                                  //     // }
                                  //   });
                                  // } else if (characteristic.isReadable) {
                                  //   characteristic.read().then((value) => print(
                                  //       '${characteristic.uuid}: $value'));
                                  // }
                                });
                              });
                            });
                          });

                          // bpmUUIDS.forEach((key, value) {
                          //   // value.forEach((element) {
                          //   connectedPeripheral
                          //       .characteristics(key)
                          //       .then((value) {
                          //     print(key);
                          //     value.forEach((characteristic) {
                          //       connectedPeripheral
                          //           .monitorCharacteristic(
                          //               key, characteristic.uuid)
                          //           .listen((event) {
                          //         print('================================');
                          //         print(characteristic.isNotifiable);
                          //         print(event.service.uuid);
                          //         print(event.uuid);
                          //         print(event.value);
                          //       });
                          //     });
                          //   });
                          //   // });
                          // });

                          // glucose.forEach((key, value) {
                          //   // value.forEach((element) {
                          //   connectedPeripheral
                          //       .characteristics(key)
                          //       .then((value) {
                          //     print(key);
                          //     value.forEach((characteristic) {
                          //       connectedPeripheral
                          //           .monitorCharacteristic(
                          //               key, characteristic.uuid)
                          //           .listen((event) {
                          //         print('================================');
                          //         print(characteristic.isNotifiable);
                          //         print(event.service.uuid);
                          //         print(event.uuid);
                          //         print(event.value);
                          //       });
                          //     });
                          //   });
                          //   // });
                          // });

                          // amazfit.forEach((key, value) {
                          //   // value.forEach((element) {
                          //   connectedPeripheral
                          //       .characteristics(key)
                          //       .then((value) {
                          //     print(key);
                          //     value.forEach((characteristic) {
                          //       connectedPeripheral
                          //           .monitorCharacteristic(
                          //               key, characteristic.uuid)
                          //           .listen((event) {
                          //         print('================================');
                          //         print(characteristic.isNotifiable);
                          //         print(event.service.uuid);
                          //         print(event.uuid);
                          //         print(event.value);
                          //       });
                          //     });
                          //   });
                          //   // });
                          // });

                          // connectedPeripheral
                          //     .monitorCharacteristic(
                          //         'cdeacb80-5235-4c07-8846-93a37ee6b86d',
                          //         'cdeacb81-5235-4c07-8846-93a37ee6b86d')
                          //     .listen((event) {
                          //   print(event.service.uuid);
                          //   print(event.uuid);
                          //   print(event.value);
                          //   if (event.value.length == 4) {
                          //     setState(() {
                          //       ppm = event.value[1];
                          //       oxyLevel = event.value[2];
                          //     });
                          //   }
                          // });

                          connectedPeripheral
                              .readCharacteristic(
                                  '0000fff0-0000-1000-8000-00805f9b34fb',
                                  '0000fff4-0000-1000-8000-00805f9b34fb')
                              .then((event) {
                            print(event.service.uuid);
                            print(event.uuid);
                            print(event.value);
                            if (event.value[0] == 30) {
                              setState(() {
                                // systol = event.value[2];
                                // diastol = event.value[4];
                                // pulse = event.value[8];
                              });
                            }
                          });

                          connectedPeripheral
                              .readCharacteristic(
                                  '0000fff0-0000-1000-8000-00805f9b34fb',
                                  '0000fff1-0000-1000-8000-00805f9b34fb')
                              .then((event) {
                            print(event.service.uuid);
                            print(event.uuid);
                            print(event.value);
                            if (event.value[0] == 30) {
                              setState(() {
                                // systol = event.value[2];
                                // diastol = event.value[4];
                                // pulse = event.value[8];
                              });
                            }
                          });

                          connectedPeripheral
                              .readCharacteristic(
                                  '0000fff0-0000-1000-8000-00805f9b34fb',
                                  '0000fff2-0000-1000-8000-00805f9b34fb')
                              .then((event) {
                            print(event.service.uuid);
                            print(event.uuid);
                            print(event.value);
                            if (event.value[0] == 30) {
                              setState(() {
                                // systol = event.value[2];
                                // diastol = event.value[4];
                                // pulse = event.value[8];
                              });
                            }
                          });

                          connectedPeripheral
                              .readCharacteristic(
                                  '0000fff0-0000-1000-8000-00805f9b34fb',
                                  '0000fff5-0000-1000-8000-00805f9b34fb')
                              .then((event) {
                            print(event.service.uuid);
                            print(event.uuid);
                            print(event.value);
                            if (event.value[0] == 30) {
                              setState(() {
                                // systol = event.value[2];
                                // diastol = event.value[4];
                                // pulse = event.value[8];
                              });
                            }
                          });

                          connectedPeripheral
                              .readCharacteristic(
                                  '0000fff0-0000-1000-8000-00805f9b34fb',
                                  '0000ff01-0000-1000-8000-00805f9b34fb')
                              .then((event) {
                            print(event.service.uuid);
                            print(event.uuid);
                            print(event.value);
                            if (event.value[0] == 30) {
                              setState(() {
                                // systol = event.value[2];
                                // diastol = event.value[4];
                                // pulse = event.value[8];
                              });
                            }
                          });

                          // connectedPeripheral
                          //     .monitorCharacteristic(
                          //         '0000fff0-0000-1000-8000-00805f9b34fb',
                          //         '0000fff4-0000-1000-8000-00805f9b34fb')
                          //     .listen((event) {
                          //   print(event.service.uuid);
                          //   print(event.uuid);
                          //   print(event.value);
                          //   if (event.value[0] == 85 &&
                          //       event.value.length == 12) {
                          //     setState(() {
                          //       glucoseLevel = event.value[9];
                          //     });
                          //   }
                          // });
                        });
                      });
                    });
                  },
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(8, 8, 16, 8),
                            child: (_isElementConnected(element) != null)
                                ? Icon(Icons.bluetooth_connected)
                                : Icon(Icons.bluetooth_disabled),
                          ),
                          Column(
                            children: [
                              Text(element.peripheral.name.toString()),
                              Text(element.rssi.toString())
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }).toList()),
              RaisedButton(
                onPressed: () {
                  _startScan();
                },
                child: Text('Start Scan'),
              ),
              RaisedButton(
                onPressed: () {
                  _stopScan();
                },
                child: Text('Stop Scan'),
              ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //   children: [Text('Oxygen level: $oxyLevel'), Text('Pulse: $ppm')],
              // ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //   children: [
              //     // Text('Systol: $systol'),
              //     // Text('Diastol: $diastol'),
              //     // Text('Pulse: $pulse'),
              //   ],
              // ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [Text('Glucose level: $glucoseLevel')],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

//sOQrGthUPQQPPTPt%7B-%2F%17%3A5%12%16%1F%1B%16%1A%0F%16.K

// I/flutter (  871): ============================
// I/flutter (  871): 00001801-0000-1000-8000-00805f9b34fb
// I/flutter (  871): 00002a05-0000-1000-8000-00805f9b34fb
// I/flutter (  871): ============================
// I/flutter (  871): 00001800-0000-1000-8000-00805f9b34fb
// I/flutter (  871): 00002a00-0000-1000-8000-00805f9b34fb
// I/flutter (  871): ============================
// I/flutter (  871): 00001800-0000-1000-8000-00805f9b34fb
// I/flutter (  871): 00002a01-0000-1000-8000-00805f9b34fb
// I/flutter (  871): ============================
// I/flutter (  871): 0000fed1-0000-3512-2118-0009af100700
// I/flutter (  871): 0000fed2-0000-3512-2118-0009af100700
// I/flutter (  871): ============================
// I/flutter (  871): 0000fee1-0000-1000-8000-00805f9b34fb
// I/flutter (  871): 0000fedd-0000-1000-8000-00805f9b34fb
// I/flutter (  871): ============================
// I/flutter (  871): 0000fee1-0000-1000-8000-00805f9b34fb
// I/flutter (  871): 0000fede-0000-1000-8000-00805f9b34fb

// I/flutter (11686): [85, 12, 3, 20, 8, 24, 9, 33, 0, 52, 0, 248]
// I/flutter (11686): 0000fff0-0000-1000-8000-00805f9b34fb
// I/flutter (11686): 0000fff4-0000-1000-8000-00805f9b34fb
// I/flutter (11686): [85, 12, 3, 84, 8, 19, 9, 29, 11, 18, 0, 24]
// I/flutter (11686): 0000fff0-0000-1000-8000-00805f9b34fb
// I/flutter (11686): 0000fff4-0000-1000-8000-00805f9b34fb
// I/flutter (11686): [85, 12, 3, 84, 8, 24, 9, 30, 18, 111, 0, 130]
// I/flutter (11686): 0000fff0-0000-1000-8000-00805f9b34fb
// I/flutter (11686): 0000fff4-0000-1000-8000-00805f9b34fb
// I/flutter (11686): [85, 12, 3, 84, 8, 24, 9, 33, 20, 52, 0, 76]
// I/flutter (11686): 0000fff0-0000-1000-8000-00805f9b34fb
// I/flutter (11686): 0000fff4-0000-1000-8000-00805f9b34fb
// I/flutter (11686): [85, 5, 5, 0, 97]

// I/flutter ( 6797): ============================
// I/flutter ( 6797): 00001800-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 00002a02-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 00001800-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 00002a03-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 00001800-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 00002a00-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 00001800-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 00002a01-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 00001800-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 00002a04-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 0000180a-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 00002a2a-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 0000180a-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 00002a23-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 0000180a-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 00002a29-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 0000180a-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 00002a24-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 0000180a-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 00002a25-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 0000180a-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 00002a27-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 0000180a-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 00002a26-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 0000180a-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 00002a28-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 0000180a-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 00002a50-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 0000fff0-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 0000fff1-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 0000fff0-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 0000fff4-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 0000fff0-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 0000fff3-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 0000fff0-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 0000fff2-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 0000fff0-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 0000fff5-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 0000ff00-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 0000ff01-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): ============================
// I/flutter ( 6797): 0000180f-0000-1000-8000-00805f9b34fb
// I/flutter ( 6797): 00002a19-0000-1000-8000-00805f9b34fb
