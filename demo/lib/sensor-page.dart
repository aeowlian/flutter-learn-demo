import 'package:flutter/material.dart';
import 'package:light/light.dart';
import 'package:sensors/sensors.dart';
import 'package:location/location.dart';

class SensorPage extends StatefulWidget {
  @override
  _SensorState createState() => _SensorState();
}

class _SensorState extends State<SensorPage> {
  Light _light;
  int lux;
  List _accelerometer;
  List _gyroscope;
  List _userAccelerometer;
  Location _location;
  String locationData;

  void listenToSensors() {
    _light = Light();
    _light.lightSensorStream.listen((event) {
      setState(() {
        lux = event;
      });
    });

    accelerometerEvents.listen((event) {
      setState(() {
        _accelerometer = [
          event.x,
          event.y,
          event.z,
        ];
      });
    });

    userAccelerometerEvents.listen((event) {
      setState(() {
        _userAccelerometer = [
          event.x,
          event.y,
          event.z,
        ];
      });
    });

    gyroscopeEvents.listen((event) {
      setState(() {
        _gyroscope = [
          event.x,
          event.y,
          event.z,
        ];
      });
    });

    // _location = new Location();
    // _location.getLocation().then((value) {
    //   setState(() {
    //     locationData = value.toString();
    //   });
    // });
    // _location.onLocationChanged.listen((event) {
    //   setState(() {
    //     locationData = event.toString();
    //   });
    // });
  }

  @override
  Widget build(BuildContext context) {
    listenToSensors();
    return Scaffold(
      body: Center(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Light intensity is: $lux lux',
                style: TextStyle(fontSize: 20),
              ),
              _accelerometer != null
                  ? Column(
                      children: [
                        Text(
                          '_accelerometer x: ${_accelerometer[0]}',
                          style: TextStyle(fontSize: 20),
                        ),
                        Text(
                          '_accelerometer y: ${_accelerometer[1]}',
                          style: TextStyle(fontSize: 20),
                        ),
                        Text(
                          '_accelerometer z: ${_accelerometer[2]}',
                          style: TextStyle(fontSize: 20),
                        ),
                      ],
                    )
                  : Container(),
              _userAccelerometer != null
                  ? Column(
                      children: [
                        Text(
                          '_userAccelerometer x: ${_userAccelerometer[0]}',
                          style: TextStyle(fontSize: 20),
                        ),
                        Text(
                          '_userAccelerometer y: ${_userAccelerometer[1]}',
                          style: TextStyle(fontSize: 20),
                        ),
                        Text(
                          '_userAccelerometer z: ${_userAccelerometer[2]}',
                          style: TextStyle(fontSize: 20),
                        ),
                      ],
                    )
                  : Container(),
              _gyroscope != null
                  ? Column(
                      children: [
                        Text(
                          '_gyroscope x: ${_gyroscope[0]}',
                          style: TextStyle(fontSize: 20),
                        ),
                        Text(
                          '_gyroscope y: ${_gyroscope[1]}',
                          style: TextStyle(fontSize: 20),
                        ),
                        Text(
                          '_gyroscope z: ${_gyroscope[2]}',
                          style: TextStyle(fontSize: 20),
                        ),
                      ],
                    )
                  : Container(),
              // locationData != null
              // Column(
              //   children: [
              //     Text(
              //       locationData,
              //       style: TextStyle(fontSize: 20),
              //     ),
              //     // Text(
              //     //   '_gyroscope y: ${_gyroscope[1]}',
              //     //   style: TextStyle(fontSize: 20),
              //     // ),
              //     // Text(
              //     //   '_gyroscope z: ${_gyroscope[2]}',
              //     //   style: TextStyle(fontSize: 20),
              //     // ),
              //   ],
              // )
              // : Container()
              // RaisedButton(
              //   onPressed: () {
              //     listenToSensors();
              //   },
              //   child: Text('Presssssss'),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
