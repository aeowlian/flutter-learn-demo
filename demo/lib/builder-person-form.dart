import 'package:demo/builder-address-form.dart';
import 'package:demo/builder-another-form.dart';
import 'package:demo/form-widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:intl/intl.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class PersonForm extends StatefulWidget {
  @override
  _PersonFormState createState() => _PersonFormState();
}

class _PersonFormState extends State<PersonForm> {
  final GlobalKey<FormBuilderState> _formState = GlobalKey<FormBuilderState>();
  var maskFormatter = new MaskTextInputFormatter(
      mask: '###.###.###.###', filter: {"#": RegExp('[0-9\.]')});

  String _getState(String attribute) {
    return _formState.currentState.fields[attribute].currentState.value;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            child: FormBuilder(
              key: _formState,
              autovalidate: true,
              onChanged: (map) {
                print(map);
              },
              child: Column(
                children: [
                  FormBuilderTextField(
                    attribute: 'name',
                    decoration: InputDecoration(
                      hintText: "John Bobdery",
                      labelText: 'Name',
                    ),
                    validators: [FormBuilderValidators.required()],
                  ),
                  FormBuilderDateTimePicker(
                    inputType: InputType.date,
                    format: DateFormat("yyyy-MMM-dd"),
                    attribute: 'dateOfBirth',
                    decoration: InputDecoration(labelText: 'Date Of Birth'),
                    onSaved: (va) {
                      print(va);
                    },
                    validators: [
                      (val) {
                        if (val != null) {
                          // DateTime date = DateTime.tryParse(val);
                          if (DateTime.now().difference(val).inDays < 6570)
                            return "You need to be over 18 to use this app!";
                        }
                      },
                    ],
                  ),
                  Container(
                    child: Column(
                      children: [
                        Divider(
                          thickness: 5,
                        ),
                        AnotherForm(
                          onChanged: (key) {
                            key.currentState.saveAndValidate();
                            print(key.currentState.value);
                          },
                          cityValidator: [
                            FormBuilderValidators.required(),
                            (value) {
                              if (_getState('name').length == 0) {
                                return "Name field is empty";
                              }
                            },
                          ],
                        ),
                        Divider(
                          thickness: 5,
                        ),
                        AddressForm(
                          onChanged: (values) {
                            print(values);
                          },
                          cityValidator: [
                            (value) {
                              if (_getState('name').length == 0) {
                                return "Name field is empty";
                              }
                            },
                          ],
                        ),
                        Divider(
                          thickness: 5,
                        ),
                      ],
                    ),
                  ),
                  FormBuilderTextField(
                    attribute: 'anotherField',
                    inputFormatters: [maskFormatter],
                    keyboardType: TextInputType.numberWithOptions(),
                    decoration: InputDecoration(
                      labelText: 'IP Address',
                    ),
                    validators: [FormBuilderValidators.IP()],
                  ),
                  FormBuilderColorPicker(
                    attribute: 'anotherOne',
                    colorPickerType: ColorPickerType.MaterialPicker,
                    decoration: InputDecoration(
                      labelText: 'Another One',
                    ),
                    validators: [FormBuilderValidators.required()],
                  ),
                  RaisedButton(
                    child: Text('Submit'),
                    onPressed: () {
                      _formState.currentState.validate();
                    },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
